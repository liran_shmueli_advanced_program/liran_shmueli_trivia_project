#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"
#include "Question.h"
#include <vector>
#include "sqlite3.h"

using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string,string);
	vector<Question> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);

	static int callbackCount(void*,int,char**,char**);
	static int callbackQuestions(void*, int, char**, char**);
	static int callbackBestScores(void*, int, char**, char**);
	static int callbackPersonalStatus(void*, int, char**, char**);
};
