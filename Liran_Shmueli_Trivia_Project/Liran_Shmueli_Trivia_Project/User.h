#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"
#include "Room.h"
#include "Game.h"

using namespace std;


class User
{

public:
	User(string,SOCKET);
	~User();
	void send(string);
	string getUsername();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearRoom();
	void clearGame();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	bool operator==(User& other);
	bool operator!=(User& other);

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;

};

