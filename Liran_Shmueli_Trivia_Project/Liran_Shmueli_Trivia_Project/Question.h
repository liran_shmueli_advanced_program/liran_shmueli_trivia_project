#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"

using namespace std;

class Question
{

public:
	Question(int,string,string,string,string,string);
	~Question();
	string getQustion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;

};

