#include <iostream>
#include "DataBase.h"
#include "Game.h"
#include "Helper.h"
#include "Protocol.h"
#include "Question.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "sqlite3.h"
#include "TriviaServer.h"
#include "User.h"
#include "Validator.h"

int main()
{
	cout << "Checking Program..." << endl;
	try
	{
		TRACE("Starting...");
		WSAInitializer wsa_init;
		TriviaServer MT;
		MT.serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in TriviaServer !" << std::endl;
	}
	system("PAUSE");
	return(0);
}