#include "Validator.h"

static bool isPasswordValid(string Password)
{
	int i = 0;
	bool check = false;
	//checks if above 4
	if (Password.length < 4)
	{
		return(false);
	}
	//checks no spaces
	for (i = 0; i < Password.length; i++)
	{
		if (Password[i] == ' ')
		{
			return(false);
		}
	}
	//checks at least one number
	check = false;
	for (i = 0; i < Password.length; i++)
	{
		if (Password[i] >= '0' && Password[i] <= '9')
		{
			check = true;
		}
	}
	if (check == false)
	{
		return(false);
	}
	//checks at least one upper letter
	check = false;
	for (i = 0; i < Password.length; i++)
	{
		if (Password[i] >= 'A' && Password[i] <= 'Z')
		{
			check = true;
		}
	}
	if (check == false)
	{
		return(false);
	}
	//checks at least one lower letter
	check = false;
	for (i = 0; i < Password.length; i++)
	{
		if (Password[i] >= 'a' && Password[i] <= 'z')
		{
			check = true;
		}
	}
	if (check == false)
	{
		return(false);
	}
	return(true);
}
static bool isUsernameValid(string Username)
{
	//checks if not empty
	if (Username.length == 0)
	{
		return(false);
	}
	//checks no spaces
	for (i = 0; i < Username.length; i++)
	{
		if (Username[i] == ' ')
		{
			return(false);
		}
	}
	//checks if starts in letter
	if ((Username[0] >= 'A' && Username[0] <= 'Z') || (Username[0] >= 'a' && Username[0] <= 'z'))
	{
		return(true);
	}
	else
	{
		return(false);
	}
}
