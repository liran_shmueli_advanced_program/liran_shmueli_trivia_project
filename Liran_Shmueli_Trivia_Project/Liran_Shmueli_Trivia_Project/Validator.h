#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"

using namespace std;

static bool isPasswordValid(string Password);
static bool isUsernameValid(string Password);
