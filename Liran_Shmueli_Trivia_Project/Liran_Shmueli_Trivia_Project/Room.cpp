#include "Room.h"

Room::Room(int  id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionsNo = questionsNo;
	this->_questionTime = questionTime;
	this->_users.push_back(admin);
}

Room::~Room()
{

}

bool Room::joinRoom(User* user)
{
	if (this->_users.size() >= this->_maxUsers)
	{
		return(false);
	}
	else
	{
		this->_users.push_back(user);
		std::vector<User*>::iterator it;
		for (it = this->_users.begin(); it != this->_users.end(); it++)
		{
			//Message to the users
		}
		return(true);
	}
}

void Room::leaveRoom(User* user)
{
	string newList;
	std::vector<User*>::iterator it;
	std::vector<User*>::iterator ot;
	for (it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it) == user)
		{
			this->_users.erase(it);
			Helper::sendData(user->getSocket(), "1120");
			newList = this->getUsersListMessage(); 
			for (ot = this->_users.begin(); ot != this->_users.end(); ot++)
			{
					Helper::sendData((*ot)->getSocket(),newList);
			}
		}

	}
}

int Room::closeRoom(User* user)
{
	if (user == this->_admin)
	{
		std::vector<User*>::iterator it;
		for (it = this->_users.begin(); it != this->_users.end(); it++)
		{
			(*it)->clearRoom();
			Helper::sendData((*it)->getSocket(), "1161");
		}
	}
	else
	{
		Helper::sendData(user->getSocket(), "1160");
		return(-1);
	}
}

vector<User*> Room::getUsers()
{
	return(this->_users);
}

string Room::getUsersListMessage()
{
	std::vector<User*>::iterator it;
	string finalMessage;
	int i = 0;
	int numOfUsers = 0;
	if (this->_users.empty())
	{
		return("1080");
	}
	else
	{
		finalMessage = finalMessage + "108";
		for (it = this->_users.begin(); it != this->_users.end(); it++)
		{
			numOfUsers++;
		}
		finalMessage = finalMessage + std::to_string(numOfUsers);
		finalMessage = finalMessage + this->getUsersAsString(this->_users, nullptr);
		return(finalMessage);
	}
}

int Room::getQuestionsNo()
{
	return(this->_questionsNo);
}

int Room::getID()
{
	return(this->_id);
}

string Room::getName()
{
	return(this->_name);
}

string Room::getUsersAsString(vector<User*> usersList, User* exludeUser)
{
	string Usernames;
	int length = 0;
	std::vector<User*>::iterator it;
	for (it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it) != exludeUser)
		{
			length = (*it)->getUsername().length;
			Usernames = Usernames + std::to_string(length) + (*it)->getUsername();
		}
	}
	return(Usernames);
}

void Room::sendMessage(string Message)
{
	sendMessage(nullptr, Message);
}

void Room::sendMessage(User* exludeUser, string Message)
{
	std::vector<User*>::iterator it;
	for (it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it) != exludeUser)
		{
			Helper::sendData((*it)->getSocket(), Message);
		}	
	}
}
