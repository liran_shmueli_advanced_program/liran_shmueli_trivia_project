#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"
#include "User.h"

using namespace std;

class Room
{
public:
	Room(int,User*,string,int,int,int);
	~Room();
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getID();
	string getName();


private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;

	string getUsersAsString(vector<User*>,User*);
	void sendMessage(string);
	void sendMessage(User*,string);
};

