#include "TriviaServer.h"

static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;

TriviaServer::TriviaServer()
{
	DataBase DB;
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

TriviaServer::~TriviaServer()
{
	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(this->_socket);
	}
	catch (...) {}
}

void TriviaServer::serve()
{
	TriviaServer::bindAndListen();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(this->_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler,this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET* client_socket)
{

}

void TriviaServer::SafeDeleteUser(RecievedMessage*)
{

}


User* handleSignIn(RecievedMessage*)
{

}

void handleSignUp(RecievedMessage*)
{

}

bool handleSignOut(RecievedMessage*)
{

}


void handleLeaveGame(RecievedMessage*)
{

}

void handleStartGame(RecievedMessage*)
{

}

void handlePlayerAnswer(RecievedMessage*)
{

}


bool handleCreateRoom(RecievedMessage*)
{

}

bool handleCloseRoom(RecievedMessage*)
{

}

bool handleJoinRoom(RecievedMessage*)
{

}

bool handleLeaveRoom(RecievedMessage*)
{

}

void handleGetUsersInRoom(RecievedMessage*)
{

}

void handleGetRooms(RecievedMessage*)
{

}


void handleGetBestScores(RecievedMessage*)
{

}

void handleGetPersonalStatus(RecievedMessage*)
{

}


void handleRecievedMessages()
{

}

void addRecievedMessage(RecievedMessage*)
{

}

RecievedMessage* bulidRecieveMessage(SOCKET, int)
{

}


User* getUserByName(string)
{

}

User* getUserBySocket(SOCKET)
{

}

Room* getRoomById(int)
{

}
