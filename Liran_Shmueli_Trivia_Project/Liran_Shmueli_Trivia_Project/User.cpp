#include "User.h"

User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
	this->_currGame = nullptr;
	this->_currRoom = nullptr;
}

User::~User()
{

}

void User::send(string Message)
{
	Helper::sendData(this->_sock,Message);
}

string User::getUsername()
{
	return(this->_username);
}

SOCKET User::getSocket()
{
	return(this->_sock);
}

Room* User::getRoom()
{
	return(this->_currRoom);
}

Game* User::getGame()
{
	return(this->_currGame);
}

void User::setGame(Game* gm)
{
	this->_currGame = gm;
	this->_currRoom = nullptr;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

void User::clearGame()
{
	this->_currGame = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		return(false);
	}
	else
	{
		this->_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
		if (this->_currRoom == nullptr)
		{
			return(false);
		}
		else
		{
			return(true);
		}
	}
}

bool User::joinRoom(Room*newRoom)
{
	if (this->_currRoom != nullptr)
	{
		return(false);
	}
	else
	{
		this->_currRoom = newRoom;
		if (this->_currRoom == nullptr)
		{
			return(false);
		}
		else
		{
			return(true);
		}
	}
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->clearRoom();
	}
}

int User::closeRoom()
{
	int ret = 0;
	if (this->_currRoom == nullptr)
	{
		return(-1);
	}
	else
	{
		ret = this->_currRoom->closeRoom(this);
		if (ret == -1)
		{
			return(-1);
		}
		else
		{
			return(ret);
		}
	}
}

bool User::leaveGame()
{

}

bool User::operator==(User& other)
{
	if (this->_username.compare(other.getUsername()) == 0)
	{
		return(true);
	}
	else
	{
		return(false);
	}
}

bool User::operator!=(User& other)
{
	if (this->_username.compare(other.getUsername()) == 0)
	{
		return(false);
	}
	else
	{
		return(true);
	}
}
