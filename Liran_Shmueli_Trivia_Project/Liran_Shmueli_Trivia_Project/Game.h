#pragma once

#include <vector>
#include <map>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"
#include "User.h"
#include "Question.h"
#include "DataBase.h"

using namespace std;




class Game
{

public:
	Game(const vector<User*>&,int,DataBase&);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User*,int,int);
	bool leaveGame(User*);
	int getID();
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;

	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

};

