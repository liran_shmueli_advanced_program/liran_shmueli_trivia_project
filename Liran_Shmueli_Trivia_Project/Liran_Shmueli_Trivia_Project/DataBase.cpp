#include "DataBase.h"

DataBase::DataBase()
{
	int rc = 0;
	sqlite3* db;
	char *zErrMsg = 0;

	//creates and opens the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		throw std::exception(__FUNCTION__ " - DataBase");
	}
	else
	{
		std::cout << "Process: DataBase Created!!!" << endl;
	}
	//close the database and finish process

}

DataBase::~DataBase()
{
	sqlite3_close(db);
	std::cout << "Process: Done!!!" << endl;
}

bool DataBase::isUserExists(string)
{

	return(true);
}

bool DataBase::addNewUser(string, string, string)
{

	return(true);
}

bool DataBase::isUserAndPassMatch(string, string)
{

	return(true);
}

vector<Question> DataBase::initQuestions(int)
{

}

vector<string> DataBase::getBestScores()
{

}

vector<string> DataBase::getPersonalStatus(string)
{

}

int DataBase::insertNewGame()
{

	return(0);
}

bool DataBase::updateGameStatus(int)
{

	return(true);
}

bool DataBase::addAnswerToPlayer(int, string, int, string, bool, int)
{

	return(true);
}

static int callbackCount(void*, int, char**, char**)
{

	return(0);
}

static int callbackQuestions(void*, int, char**, char**)
{

	return(0);
}

static int callbackBestScores(void*, int, char**, char**)
{

	return(0);
}

static int callbackPersonalStatus(void*, int, char**, char**)
{

	return(0);
}
