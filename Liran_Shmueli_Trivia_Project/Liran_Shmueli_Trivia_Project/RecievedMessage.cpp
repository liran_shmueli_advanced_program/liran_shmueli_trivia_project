#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int  messageCode)
{
	this->_messageCode = messageCode;
	this->_sock = sock;
}

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode, vector<string> values)
{
	this->_messageCode = messageCode;
	this->_sock = sock;
	this->_values = values;
}

RecievedMessage::~RecievedMessage()
{
	
}

SOCKET RecievedMessage::getSock()
{
	return(this->_sock);
}

User* RecievedMessage::getUser()
{
	return(this->_user);
}

void RecievedMessage::setUser(User* newUser)
{
	this->_user = newUser;
}

int RecievedMessage::getMessageCode()
{
	return(this->_messageCode);
}

vector<string>& RecievedMessage::getValues()
{
	return(this->_values);
}
