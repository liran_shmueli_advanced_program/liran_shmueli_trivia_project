#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include "Helper.h"
#include "User.h"

using namespace std;

class RecievedMessage
{

public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);
	~RecievedMessage();
	SOCKET getSock();
	User* getUser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};

