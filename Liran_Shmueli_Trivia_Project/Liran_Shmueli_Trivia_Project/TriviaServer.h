#pragma once

#include <thread>
#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include <mutex>
#include <queue>
#include "DataBase.h"
#include "Game.h"
#include "Helper.h"
#include "Protocol.h"
#include "Question.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "sqlite3.h"
#include "User.h"
#include "Validator.h"
#include "WSAInitializer.h"

using namespace std;




class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve();

private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> roomsList;
	mutex mtxRecievedMessages;
	queue<RecievedMessage*> queRcvMessage;
	static int _roomIdSequence;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET*);
	void SafeDeleteUser(RecievedMessage*);

	User* handleSignIn(RecievedMessage*);
	void handleSignUp(RecievedMessage*);
	bool handleSignOut(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);
	
	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);
	
	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* bulidRecieveMessage(SOCKET,int);

	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
};
